#include "neuronlayer.h"

neuronLayer::neuronLayer()
{

}

neuronLayer::neuronLayer(int a,int b)
{
    //gen = new mt19937(rd());
    //dis = new uniform_real_distribution<>(-1, 1);
    NEUR_NUM = b;
    INPUTS = a;
    inputSignals = (double *)malloc(INPUTS*sizeof(double));
    inputs = INPUTS;
    output = (double *)malloc(NEUR_NUM*sizeof(double));
    coef = (double **)malloc(NEUR_NUM*sizeof(double));
    for (int i=0;i<NEUR_NUM;i++){
        coef[i] = (double *)malloc(INPUTS*sizeof(double));
    }
    step = (double *)malloc(NEUR_NUM*sizeof(double));
    deltaForPrevLayer = (double *)malloc(NEUR_NUM*sizeof(double));
}

void neuronLayer::init()
{
    for(int i=0;i<NEUR_NUM;i++){
        for(int j=0;j<INPUTS;j++){
        //coef[i][j]=(*dis)(*gen);
            coef[i][j]=((double)(rand()%2000)/1000)-1;
        }

        step[i]=((double)(rand()%2000)/1000)-1;
        //cout<<"step"<<step[i]<<endl;
        //step[i]=(*dis)(*gen);
    }
}

double *neuronLayer::pass()
{
    for(int i=0;i<NEUR_NUM;i++){
        double sum=0;
        for(int j=0;j<INPUTS;j++){
            sum+=inputSignals[j]*coef[i][j];
        }
        //output[i]=sum;
        sum+=step[i];
        output[i]=1/(1+exp(-sum));
    }
    return output;
}

void neuronLayer::learn(double * input,double * input2)//input2 - требуемый выход
{
    for(int i=0;i<NEUR_NUM;i++){
        //deltaForPrevLayer[i]=0;
        for(int j=0;j<INPUTS;j++){
        coef[i][j]=coef[i][j]+exitLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i])*input[j];

        }
        step[i]=step[i]+exitLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i]);
       deltaForPrevLayer[i]=exitLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i]);
    }
   // cout<<"exit error "<<input2[0]-output[0]<<endl;
}
void neuronLayer::learnHidden(double * input, double * delta, double nextCoef[5][36])
{
    double input2[NEUR_NUM];//требуемый выход
    double errorSum[NEUR_NUM];
    for(int j=0;j<NEUR_NUM;j++){
        errorSum[j]=0;
        for(int i=0;i<5;i++){
            errorSum[j]+=delta[i]*nextCoef[i][j];
        }
        input2[j]=errorSum[j]+output[j];
    }

    for(int i=0;i<NEUR_NUM;i++){
        //deltaForPrevLayer[i]=0;
        for(int j=0;j<INPUTS;j++){
        coef[i][j]=coef[i][j]+hiddenLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i])*input[j];
        }
        step[i]=step[i]+hiddenLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i]);
       //deltaForPrevLayer[i]=hiddenLearnSpeed*output[i]*(1-output[i])*(input2[i]-output[i]);
    }
   // cout<<"hidden error "<<input2[0]-output[0]<<endl;
}
