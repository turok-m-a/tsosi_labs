#include <iostream>
#include <neuronlayer.h>
#include <iostream>
#include <iomanip>
#include <warp.h>
#include <bmp.h>
using namespace std;
double ti[5] = {1,0,0,0,0};
double li[5] = {0,1,0,0,0};
double ui[5] = {0,0,1,0,0};
double oi[5] = {0,0,0,1,0};
double ki[5] = {0,0,0,0,1};
vector<int> v;
double coefPod(const double *, double*);
void showImg(double * img);

int main()
{

    double * t = ldToBin(L"T0.bmp");
    double * l = ldToBin(L"L0.bmp");
    double * u = ldToBin(L"U0.bmp");
    double * o = ldToBin(L"O0.bmp");
    double * k = ldToBin(L"K0.bmp");

    cout<<endl;
    double * tNoise = ldToBin(L"T3.bmp");
    double * lNoise = ldToBin(L"L3.bmp");
    double * uNoise = ldToBin(L"U3.bmp");
    double * Noise = ldToBin(L"NOISE.bmp");
    srand(time(0));
    warp w(0);
    w.addLearnImage(t,ti);
    w.addLearnImage(l,li);
    w.addLearnImage(u,ui);
    w.addLearnImage(o,li);
    w.addLearnImage(k,ui);
    w.learn();

    showImg(tNoise);
    const double * e = w.process(tNoise);

    cout<<"T: "<<e[0]<<endl;
     cout<<"L: "<<e[1]<<endl;
      cout<<"U: "<<e[2]<<endl;
      cout<<"O: "<<e[3]<<endl;
       cout<<"K: "<<e[4]<<endl;
    showImg(lNoise);
    e = w.process(lNoise);

//    showImg((double *)e);

    cout<<"T: "<<e[0]<<endl;
     cout<<"L: "<<e[1]<<endl;
      cout<<"U: "<<e[2]<<endl;
      cout<<"O: "<<e[3]<<endl;
       cout<<"K: "<<e[4]<<endl;
    showImg(uNoise);
    e = w.process(uNoise);

//    showImg((double *)e);

    cout<<"T: "<<e[0]<<endl;
     cout<<"L: "<<e[1]<<endl;
      cout<<"U: "<<e[2]<<endl;
      cout<<"O: "<<e[3]<<endl;
       cout<<"K: "<<e[4]<<endl;


    showImg(Noise);
    e =w.process(Noise);
    cout<<"T: "<<e[0]<<endl;
     cout<<"L: "<<e[1]<<endl;
      cout<<"U: "<<e[2]<<endl;
      cout<<"O: "<<e[3]<<endl;
       cout<<"K: "<<e[4]<<endl;
    return 0;
}

//double coefPod(const double*a,double*b){
//    double c = 0;
//    for(int i=0;i<INPUTS;i++){
//        if ((a[i]>0.5)&&(b[i]>0.5)||(a[i]<=0.5)&&(b[i]<=0.5)){
//            c++;
//        }
//    }
//    return c/INPUTS;
//}

void showImg(double * img){
    for(int i=0;i<6;i++){
        for(int j=0;j<6;j++){

            if(img[i*6+j]>0.5){
                cout<<"X";
            } else {
                cout<<" ";
            }
        }
        cout<<endl;
    }
    cout<<endl;
}



//double l[36] = {
//    1,1,0,0,0,0,
//    1,1,0,0,0,0,
//    1,1,0,0,0,0,
//    1,1,0,0,0,0,
//    1,1,1,1,1,1,
//    1,1,1,1,1,1
//};
//const double lNoise[36] = {
//    1,1,0,0,1,0,
//    1,1,0,1,1,0,
//    1,1,0,1,0,0,
//    1,1,0,0,0,0,
//    1,1,1,1,1,1,
//    1,1,1,1,1,1
//};
//double u[36] = {
//    1,1,0,0,1,1,
//    1,1,0,0,1,1,
//    1,1,0,0,1,1,
//    1,1,0,0,1,1,
//    1,1,1,1,1,1,
//    1,1,1,1,1,1
//};
//const double uNoise[36] = {
//    1,1,0,0,1,1,
//    1,1,0,1,1,1,
//    1,1,0,1,1,1,
//    1,1,0,0,1,1,
//    1,1,0,1,1,1,
//    1,1,1,1,1,1
//};
//double t[36] = {
//    1,1,1,1,1,1,
//    1,1,1,1,1,1,
//    0,0,1,1,0,0,
//    0,0,1,1,0,0,
//    0,0,1,1,0,0,
//    0,0,1,1,0,0,
//};
//const double tNoise[36] = {
//    1,1,1,1,1,1,
//    1,1,1,0,1,1,
//    0,0,1,1,0,0,
//    1,0,1,1,0,0,
//    0,0,0,1,0,1,
//    0,0,1,1,0,0,
//};
//const double Noise[36] = {
//    1,0,0,1,0,1,
//    1,0,0,1,0,0,
//    1,0,1,0,1,0,
//    1,1,0,1,0,1,
//    0,1,0,1,0,1,
//    0,0,1,1,0,0,
//};
