TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    neuronlayer.cpp \
    warp.cpp \
    bmp.cpp

HEADERS += \
    neuronlayer.h \
    warp.h \
    bmp.h
