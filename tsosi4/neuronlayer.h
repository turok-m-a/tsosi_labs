#ifndef NEURONLAYER_H
#define NEURONLAYER_H
//#define INPUTS 36
//#define NEUR_NUM 36
#include <ctime>
#include <random>
#include <iostream>
#include <math.h>
using namespace std;
class neuronLayer
{
private:
    int INPUTS;
    int NEUR_NUM;
    //random_device rd;
    //mt19937 * gen;
    //uniform_real_distribution<> * dis;
    double dabs(double a){
        if (a>=0) return a;
        return -a;
    }
    double exitLearnSpeed = 0.7;
    double hiddenLearnSpeed = 0.7;
public:
    neuronLayer ();
    neuronLayer(int a, int b);
    void init();
    double * inputSignals;
    short inputs;
    double * output;
    double ** coef;//[NEUR_NUM][INPUTS];
    double * step;
    double * deltaForPrevLayer;
    double *pass();
    void learn(double *, double *input2);
    void learnHidden(double *, double *input2, double[5][36]);
};

#endif // NEURONLAYER_H
