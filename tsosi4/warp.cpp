#include "warp.h"

warp::warp(int inputs)
{
    //hidden = neuronLayer(36,36);
    //exit = neuronLayer(36,5);
    hidden.init();
    exit.init();
}

const double *warp::process(const double *input)
{
    memcpy(hidden.inputSignals,input,sizeof(double)*hidden.inputs);
    hidden.pass();
    memcpy(exit.inputSignals,hidden.output,sizeof(double)*hidden.inputs);
    exit.pass();
    return exit.output;
}

void warp::addLearnImage(double * input, double *output)
{
    in.push_back(input);
    out.push_back(output);
}

void warp::learn()
{
    double coefTmp[5][36];
    for(int i=0;i<200;i++){
        for (int j=0;j<in.size();j++){
            memcpy(hidden.inputSignals,in[j],sizeof(double)*hidden.inputs);
            hidden.pass();
            memcpy(exit.inputSignals,hidden.output,sizeof(double)*hidden.inputs);
            exit.pass();
//            for(int i=0;i<6;i++){
//                for(int j=0;j<6;j++){
//                    cout<<exit.output[i*4+j]<<"  ";
//                }
//                cout<<endl;
//            }
            //memcpy(coefTmp,exit.coef,5*36*sizeof(double));
            for(int a=0;a<5;a++){
                for (int b=0;b<36;b++){
                    coefTmp[a][b] = exit.coef[a][b];
                }
            }
            exit.learn(exit.inputSignals,out[j]);
            hidden.learnHidden(hidden.inputSignals,exit.deltaForPrevLayer,coefTmp);
        }
    }
}
