#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->grayScale->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loadButton_clicked()
{
    if (image) {
        //delete[] image;
    }
    QString name = ui->loadName->toPlainText();
    wchar_t wname[255];
    name.toWCharArray(wname);
    wname[name.length()] = 0;
    image = LoadBMP(&width,&height,&size,(LPCTSTR)wname);
    if (4 - ((width * 3) % 4)){
        padding = 4 - ((width * 3) % 4);
    }
    if (image==NULL) {
        return;
    }
    on_grayScale_clicked();
    drawPlot();
    src.load(name);
    src=src.scaledToHeight(341,Qt::TransformationMode::SmoothTransformation);
    ui->label_4->setPixmap(src);

}

void MainWindow::on_prepD_clicked()
{
    if (!image) return;
    imageOutput = (BYTE*)malloc(size);
    memcpy(imageOutput,image,size);
    bool ok1,ok2;
    minPrep=ui->lineEdit->text().toInt(&ok1);
    maxPrep=ui->lineEdit_2->text().toInt(&ok2);
    for (int i = 0;i<size && ok1 && ok2;i++)
    {
        imageOutput[i]=image[i]*(maxPrep-minPrep)/255+minPrep;
    }
    SaveBMP(imageOutput,width,height,size,L"output.bmp");
    free(imageOutput);
    showOutput();
}


void MainWindow::on_preparE_clicked()
{
    if (!image) return;
    imageOutput = (BYTE*)malloc(size);
    memcpy(imageOutput,image,size);
    bool ok1,ok2;
    minPrep=ui->lineEdit->text().toInt(&ok1);
    maxPrep=ui->lineEdit_2->text().toInt(&ok2);
    for (int i = 0;i<size &&ok1 &&ok2;i++){
        if (image[i]<minPrep){
            imageOutput[i]=0;
        } else if (image[i]>maxPrep){
            imageOutput[i]=255;
        } else {
            imageOutput[i]=(image[i]-minPrep)*255/(maxPrep-minPrep);
        }

    }

    SaveBMP(imageOutput,width,height,size,L"output.bmp");
    //free(imageOutput);
    image = imageOutput;
    showOutput();
}
void MainWindow::on_PrepE_clicked(){}

void MainWindow::on_grayScale_clicked()
{
    if (!image) return;
    imageOutput = (BYTE*)malloc(size);
    memcpy(imageOutput,image,size);
    for (int i = 0;i<size;i+=3){
        imageOutput[i]=image[i]*0.3+image[i+1]*0.59+image[i+2]*0.11;
        imageOutput[i+1]=imageOutput[i];
        imageOutput[i+2]=imageOutput[i];
    }
    //SaveBMP(imageOutput,width,height,size,L"output.bmp");
    //free(imageOutput);
    image = imageOutput;
    showOutput();
}

void MainWindow::on_minFiltr_clicked()
{
    if (!image) return;
    imageOutput = (BYTE*)malloc(size);
    memcpy(imageOutput,image,size);
    int rgbBytes = 1;
    if (!isBW()) {
        rgbBytes = 3;
        for (int i = 0;i<height;i++){
            for (int j = 0;j<width;j++){
            imageOutput[i*rgbBytes*width+j*rgbBytes+1]=minFilter(image,j,i,width,height,1,rgbBytes);
            imageOutput[i*rgbBytes*width+j*rgbBytes+2]=minFilter(image,j,i,width,height,2,rgbBytes);
            }
        }
    }
    for (int i = 0;i<height;i++){
        for (int j = 0;j<width;j++){
        imageOutput[i*rgbBytes*width+j*rgbBytes]=minFilter(image,j,i,width,height,0,rgbBytes);
        }
    }
    SaveBMP(imageOutput,width,height,size,L"output.bmp");
    //free(imageOutput);
    image = imageOutput;
    showOutput();
}

void MainWindow::on_maxFiltr_clicked()
{
    if (!image) return;
    imageOutput = (BYTE*)malloc(size);
    memcpy(imageOutput,image,size);
    int rgbBytes = 1;
    if (!isBW()) {
        rgbBytes = 3;
        for (int i = 0;i<height;i++){
            for (int j = 0;j<width;j++){
                imageOutput[i*rgbBytes*width+j*rgbBytes+1]=maxFilter(image,j,i,width,height,1,rgbBytes);
                imageOutput[i*rgbBytes*width+j*rgbBytes+2]=maxFilter(image,j,i,width,height,2,rgbBytes);
            }
        }
    }
    for (int i = 0;i<height;i++){
        for (int j = 0;j<width;j++){
        imageOutput[i*rgbBytes*width+j*rgbBytes]=maxFilter(image,j,i,width,height,0,rgbBytes);
        }
    }

    SaveBMP(imageOutput,width,height,size,L"output.bmp");
    //free(imageOutput);
    image = imageOutput;
    showOutput();
}

void MainWindow::on_minmaxFiltr_clicked()
{
    if (!image) return;
    imageOutput = (BYTE*)malloc(size);
    memcpy(imageOutput,image,size);
    BYTE * tmpImage = (BYTE*)malloc(size);
    int rgbBytes = 1;
    if (!isBW()) {
        rgbBytes = 3;
        for (int i = 0;i<height;i++){
            for (int j = 0;j<width;j++){
            tmpImage[i*rgbBytes*width+j*rgbBytes+1]=minFilter(image,j,i,width,height,1,rgbBytes);
            tmpImage[i*rgbBytes*width+j*rgbBytes+2]=minFilter(image,j,i,width,height,2,rgbBytes);
            }
        }
    }
    for (int i = 0;i<height;i++){
        for (int j = 0;j<width;j++){
        tmpImage[i*rgbBytes*width+j*rgbBytes]=minFilter(image,j,i,width,height,0,rgbBytes);

        }
    }
    if (!isBW()) {
        for (int i = 0;i<height;i++){
            for (int j = 0;j<width;j++){
                imageOutput[i*rgbBytes*width+j*rgbBytes+1]=maxFilter(tmpImage,j,i,width,height,1,rgbBytes);
                imageOutput[i*rgbBytes*width+j*rgbBytes+2]=maxFilter(tmpImage,j,i,width,height,2,rgbBytes);
            }
        }
    }
    for (int i = 0;i<height;i++){
        for (int j = 0;j<width;j++){
        imageOutput[i*rgbBytes*width+j*rgbBytes]=maxFilter(tmpImage,j,i,width,height,0,rgbBytes);

        }
    }
    SaveBMP(imageOutput,width,height,size,L"output.bmp");
    free(tmpImage);
    free(imageOutput);
    showOutput();
}
void MainWindow::drawPlot(){
    int a=0;
    int b=255;
    int rgbOffset = 3;
    int colorPixelSize = height*width;
    on_grayScale_clicked();
    QVector<double> histogrammData(256),x(256);
    for (int i=0;i<256;i++){
        x[i]=i;
    }
    if (isBW()){
        rgbOffset = 1;
        ui->green->clearGraphs();
        ui->green->replot();
        ui->blue->clearGraphs();
        ui->blue->replot();
    }
    for (int i=0;i<colorPixelSize;i++){
        histogrammData[image[i*rgbOffset]]++;
    }
    ui->red->clearGraphs();
    ui->red->addGraph();
    ui->red->graph(0)->setData(x,histogrammData);
    ui->red->xAxis->setRange(a,b);
    double maxCount=0;
    short int firstPeak,secondPeak;
    for (int i=0;i<256;i++){
        if (histogrammData[i]>maxCount){
            maxCount = histogrammData[i];
            firstPeak = i;
        }
    }
    ui->red->yAxis->setRange(0,maxCount);
    ui->red->replot();
    short int histWidth=0;
    int left,right;
    do {
        maxCount = 0;
        if (firstPeak+histWidth>255){
            right=255;
        } else {
            right=firstPeak+histWidth;
        }
        if (firstPeak-histWidth<0){
            left=0;
        } else {
            left=firstPeak-histWidth;
        }
        histogrammData[right]=0;
        histogrammData[left]=0;
        histWidth++;

        for (int i=0;i<256;i++){
            if (histogrammData[i]>maxCount){
                maxCount = histogrammData[i];
                secondPeak = i;
            }
        }



    } while (!(secondPeak > firstPeak +1+histWidth || secondPeak < firstPeak - histWidth - 1));
    cout << firstPeak << endl << secondPeak <<endl;
    short int step = abs((firstPeak-secondPeak))/2;
    if (firstPeak>secondPeak){
        step+=secondPeak;
    } else {
        step+=firstPeak;
    }
    ui->lineEdit->setText(QString::number(step));
    ui->lineEdit_2->setText(QString::number(step+1));
    on_preparE_clicked();
    //return;
    //byte * monochrome = (byte*)calloc(size/3,1);
    //for (int i=0;i<size;i+=3){
    //    if (image[i]) {
    //    monochrome[i/3]=1;
    //    }
    //}
    int filltN = ui->lineEdit_4->text().toInt();
    for (int i=0;i<filltN;i++)
    on_minFiltr_clicked();


    for (int i=0;i<1;i++)
    on_maxFiltr_clicked();
    byte ** xyMono =new byte*[width];
    for (int i=0;i<width;i++){
        xyMono[i] =new byte[height];
    }
    for (int y=0;y<height;y++){
        for (int x=0;x<width;x++){
            if(image[width*y*3+x*3] == 255)
            xyMono[x][y]=1;
            else
            xyMono[x][y]=0;
        }
    }

    labels = new unsigned *[width];
    for (int i=0;i<width;i++){
        labels[i] = (unsigned*)calloc(height,sizeof(unsigned));
        //labels[i] = new unsigned[height];
    }



    for (int y=0;y<height;y+=10){
        for (int x=0;x<width;x+=10){
            if((x == 0) || (x == width)||(y == 0) || (y == height)){
                xyMono[x][y]=0;
            }

        }
    }
    labeling(xyMono,labels);
    int del;
    for (int y=0;y<height;y+=10){
        for (int x=0;x<width;x+=10){

            if (xyMono[x][y]){
            cout << "X";//labels[x][y];
            del = labels[x][y];
            }
            else
            cout << " ";
        }

        cout <<del << endl;
        cout.flush();
    }
    int x1,x2,y1,y2;
    set<unsigned> labelList;
    for (int y=0;y<height;y++)
        for (int x=0;x<width;x++){
        if (labels[x][y]){
            labelList.insert(labels[x][y]);

        }
    }
    set<unsigned>::iterator cur = labelList.begin();
    for (int i=0;i<labelList.size();i++){
        //cout <<i<<": "<<x<< *cur << endl;
        cur++;
    }
    //perimetr(8394,x1,x2,y1,y2);
    //object[4]; //периметр, площадь, компактность
    objects = (double**)malloc(labelList.size()*sizeof(double*));


    cur = labelList.begin();
        for (int i=0;i<labelList.size();i++){
            objects[i] = (double*)malloc(sizeof(double)*5);
            //cur++;
        }
        labelNums = (int*)malloc(sizeof(int)*labelList.size());
        params(objects,labelList);

    //k-средних

        int K=ui->lineEdit_3->text().toInt();
        bool flag = true;
        //kmeans.run(points);
            vector<Point> points;
            for(int i = 0; i < labelList.size(); i++)
                {
                    vector<double> values;

                    for(int j = 0; j < 5; j++)
                    {

                        values.push_back(objects[i][j]);
                    }


                        Point p(i, values);
                        points.push_back(p);

                }
            KMeans kmeans(K,labelList.size(), 5, 5);
            kmeans.run(points);
            flag = kmeans.moreKl();
            for (int i = 0;i<K;i++){
                int ptsNum = kmeans.clusters[i].getTotalPoints();
                for (int j = 0;j<ptsNum;j++){
                    int ln = findZ(kmeans.clusters[i].getPoint(j).getValue(0),labelList.size());
                    int x1,x2,y1,y2;
                    perimetr(labelNums[ln],x1,x2,y1,y2);
                    for (int y=y1;y<y2;y++){
                        for (int x=x1;x<x2;x++){
                            image[(y*width+x)*3+(i%3)]=(50+(i/3*150));
                        }
                   }
                }
            }


        SaveBMP(image,width,height,size,L"output.bmp");
        showOutput();

}

int MainWindow::findZ(double param,int l){
    for (int i=0;i<l;i++){
        if (param== objects[i][0])
            return i;
    }
    return 0;
}
void MainWindow::showOutput(){
    outputImage.load("output.bmp");
    outputImage= outputImage.scaledToHeight(341,Qt::TransformationMode::SmoothTransformation);
    ui->label_5->setPixmap(outputImage);
}


void MainWindow::params(double ** objects,set<unsigned> & labelList){
    set<unsigned>::iterator cur = labelList.begin();
    for (int i=0;i<labelList.size();i++){

            int count_pl = 0;
            int count_pr = 0;
            int flag = 0;
            int flag2 = 0;
            int cord1 = 0;
            int cord2 = 0;
            int side1 = 0;
            int side2 = 0;
            int x1,x2,y1,y2;
            int l = *cur;
            labelNums[i]=l;
            perimetr(l,x1,x2,y1,y2);
            for (int y=y1;y<y2;y++)
                for (int x=x1;x<x2;x++){
                if ((labels[x][y] == l)){

                    count_pl++;
                    if (!(labels[x-1][y] && labels[x+1][y] && labels[x][y+1] && labels[x][y-1] &&
                          labels[x-1][y-1] && labels[x+1][y-1] && labels[x+1][y+1] && labels[x-1][y+1]
                          )){ //если пиксель - крайний в объекте
                        count_pr++;
                    }
                    //if (!labels[x][y-1] && labels[x][y+1] && labels[x+1][y]){ //side1
                    if (!labels[x][y-1]){ //side1
                        side1++;
                    }
                    //if (!labels[x-1][y]  && labels[x][y-1] && labels[x+1][y]){ //side2
                    if (!labels[x-1][y]){ //side2
                        side2++;
                    }
                }
                }

            int f1 = (x2-x1)/2+x1;
            int y;
            for (y=y1;y<y2;y++){
                if (labels[f1][y] == l) {
                    flag = 1;
                    break;
                }
            }
            y++;
            for (y;y<y2;y++){
                if ((labels[f1][y-1] != l)  && labels[f1][y]) {
                        flag2 = 1;
                        break;
                    }
             }

//            int g1 = (y2-y1)/2+y1;
//            int x;
//            for (x=x1;x<x2;x++){
//                if (labels[x][g1] == l) {
//                    flag = 1;
//                    break;
//                }
//            }
//            x++;
//            for (x;x<x2;x++){
//                if ((labels[x-1][g1] != l)  && labels[x][g1]) {
//                        flag2 = 1;
//                        break;
//                    }
//             }


      objects[i][0]=(double)count_pl;
      objects[i][1]=(double)count_pr;
      objects[i][2]=(double)count_pr*count_pr/count_pl*2;
      objects[i][3]=(double)(sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) ))/ count_pl;//((x2-x1)); //*(y2-y1)
      //objects[i][3]=((double)side1)/((double)side2);
      //objects[i][3]=count_pl*count_pr*count_pr*side1/side2;
      //objects[i][4] = flag2*10000;
      objects[i][4] = max(side1/(side2+0.000001),side2/(side1+0.000001));
      if (objects[i][0] != 0)
      cout << i << " " << objects[i][0] << " " << objects[i][1] << " " << objects[i][2] << " " << objects[i][3] << endl;
      cur++;
    }


}
void MainWindow::labeling(byte* img[],  unsigned * labels[])
{
 int L = 0;
 for(int y = 0; y < height; ++y)
  for(int x = 0; x < width; ++x)
   fill(img, labels, x, y, ++L);
}
void MainWindow::fill(byte* img[],  unsigned *labels[], int x, int y, int L)
{
  if( (labels[x][y] == 0) & (img[x][y] == 1) )
  {
    labels[x][y] = L;
    if( x > 0 )
        fill(img, labels, x-1, y, L);
    if( x < width -1 )
        fill(img, labels, x+1, y, L);
    if( y > 0 )
        fill(img, labels, x, y-1, L);
    if( y < height-1 )
        fill(img, labels, x, y+1, L);
  }
}

void MainWindow::perimetr(unsigned label,int &x1,int &x2,int &y1,int &y2){
    for (int y=0;y<height;y++){
        for (int x=0;x<width;x++){
            if (labels[x][y]==label){
                y1=y;
                y=height;
                break;
            }
        }
    }

    for (int y=y1;y<height;y++){
        for (int x=0;x<width;x++){
            if (labels[x][y]==label){
                y2=y;
            }
        }
    }
    for (int x=0;x<width;x++){
        for (int y=0;y<height;y++){
            if (labels[x][y]==label){
                x1=x;
                x=width;
                break;
            }
        }
    }
    //cout << label <<endl;
    for (int x=x1;x<width;x++){
        for (int y=0;y<height;y++){
            if (labels[x][y]==label){
                x2=x;
            }
        }
    }


}
