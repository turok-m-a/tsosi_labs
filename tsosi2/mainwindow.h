#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <bmp.h>
#include <iostream>
#include <set>
#include <kmeans.h>
using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_loadButton_clicked();

    void on_prepD_clicked();

    void on_PrepE_clicked();

    void on_preparE_clicked();

    void on_grayScale_clicked();

    void on_minFiltr_clicked();

    void on_maxFiltr_clicked();

    void on_minmaxFiltr_clicked();

private:
    double dmin(double a,double b){
        if (a<b) return a;
        else return b;
    }
    double dmax(double a,double b){
        if (a>b) return a;
        else return b;
    }
    double ** objects;
    int * labelNums;
    void params(double ** objects, set<unsigned> &uniq);
    int findZ(double params,int l);
    unsigned ** labels;
    void labeling(byte* img[], unsigned *labels[]);
    void fill(byte* img[], unsigned *labels[], int x, int y, int L);
    void perimetr(unsigned label, int &x1, int &x2, int &y1, int &y2);
    Ui::MainWindow *ui;
    BYTE * image = NULL;
    BYTE * imageOutput;
    int width, height;
    long size;
    QImage view;
    void drawPlot();
    QPixmap src;
    QPixmap outputImage;
    void showOutput();
    int minPrep = 40;
    int maxPrep = 216;
    int padding = 0;
};

#endif // MAINWINDOW_H
