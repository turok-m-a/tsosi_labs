
#include <windows.h>
#include <stdio.h>       // for memset


bool SaveBMP ( BYTE* Buffer, int width, int height, long paddedsize, LPCTSTR bmpfile );
BYTE* LoadBMP ( int* width, int* height, long* size, LPCTSTR bmpfile );

int minFilter(const byte * img, int x, int y, int width, int height, int offset, int pixelBytes);
int maxFilter(const byte *img, int x, int y, int width, int height, int offset, int pixelBytes);
void SaveBW(BYTE * Buffer, int width, int height, long paddedsize, LPCTSTR bmpfile);
BYTE * LoadBW(char * name, int size);
bool isBW();

