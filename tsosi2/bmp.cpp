
/*************************************************************************
	
	file created on:	2002/08/30   19:33
	filename: 			Bmp.cpp
	author:				Andreas Hartl

		visit http://www.runicsoft.com for updates and more information

	purpose:	functions to load raw bmp data,
	                      to save raw bmp data,
						  to convert RGB data to raw bmp data,
						  to convert raw bmp data to RGB data
						  and to use the WinAPI to select
							a bitmap into a device context

	file updated on 2010/09/13
		in the 8 years since i first wrote this the windows file functions
		have changed their input from char* to LPCTSTR. 
		Updated this in the code here

**************************************************************************/

#include "bmp.h"
char * bwHeader[0x436];       // заголовок grayscale
bool bw = false;


int minFilter(const byte *img, int x, int y,int width,int height,int offset,int pixelBytes){
    unsigned char neighbours[9] = {255,255,255,255,255,255,255,255,255};    //0 1 2
    int pos = y*width*pixelBytes + x*pixelBytes;                                          //7 8 3
    if (y>0 && y<height-1) {                                            //6 5 4
        neighbours[1]=img[pos-width*pixelBytes+offset];
        neighbours[5]=img[pos+width*pixelBytes+offset];
        if (x>0) {
            neighbours[0]=img[pos-width*pixelBytes-pixelBytes+offset];
            neighbours[6]=img[pos+width*pixelBytes-pixelBytes+offset];
            neighbours[7]=img[pos-pixelBytes+offset];
        }
        if (x<width-1) {
            neighbours[2]=img[pos-width*pixelBytes+pixelBytes+offset];
            neighbours[3]=img[pos+pixelBytes+offset];
            neighbours[4]=img[pos+width*pixelBytes+pixelBytes+offset];
        }
    }
    if (y==0) {
        neighbours[5]=img[pos+width*pixelBytes+offset];
        if (x>0) {
            neighbours[6]=img[pos+width*pixelBytes-pixelBytes+offset];
            neighbours[7]=img[pos-pixelBytes+offset];
        }
        if (x<width-1) {
            neighbours[3]=img[pos+pixelBytes+offset];
            neighbours[4]=img[pos+width*pixelBytes+pixelBytes+offset];
        }
    }
    if (y==height-1) {
        neighbours[1]=img[pos-width*pixelBytes+offset];
        if (x>0) {
            neighbours[0]=img[pos-width*pixelBytes-pixelBytes+offset];
            neighbours[7]=img[pos-pixelBytes+offset];
        }
        if (x<width-1) {
            neighbours[2]=img[pos-width*pixelBytes+pixelBytes+offset];
            neighbours[3]=img[pos+pixelBytes+offset];
        }
    }
    neighbours[8]=img[pos+offset];
    int min = neighbours[0];
    for (int i=1;i<=8;i++){
        if (neighbours[i]<min){
            min = neighbours[i];
        }
    }
    return min;
}


int maxFilter(const byte *img, int x, int y,int width,int height,int offset,int pixelBytes){
    unsigned char neighbours[9] = {0,0,0,0,0,0,0,0,0};    //0 1 2
    int pos = y*width*pixelBytes + x*pixelBytes;                                          //7 8 3
    if (y>0 && y<height-1) {                                            //6 5 4
        neighbours[1]=img[pos-width*pixelBytes+offset];
        neighbours[5]=img[pos+width*pixelBytes+offset];
        if (x>0) {
            neighbours[0]=img[pos-width*pixelBytes-pixelBytes+offset];
            neighbours[6]=img[pos+width*pixelBytes-pixelBytes+offset];
            neighbours[7]=img[pos-pixelBytes+offset];
        }
        if (x<width-1) {
            neighbours[2]=img[pos-width*pixelBytes+pixelBytes+offset];
            neighbours[3]=img[pos+pixelBytes+offset];
            neighbours[4]=img[pos+width*pixelBytes+pixelBytes+offset];
        }
    }
    if (y==0) {
        neighbours[5]=img[pos+width*pixelBytes+offset];
        if (x>0) {
            neighbours[6]=img[pos+width*pixelBytes-pixelBytes+offset];
            neighbours[7]=img[pos-pixelBytes+offset];
        }
        if (x<width-1) {
            neighbours[3]=img[pos+pixelBytes+offset];
            neighbours[4]=img[pos+width*pixelBytes+pixelBytes+offset];
        }
    }
    if (y==height-1) {
        neighbours[1]=img[pos-width*pixelBytes+offset];
        if (x>0) {
            neighbours[0]=img[pos-width*pixelBytes-pixelBytes+offset];
            neighbours[7]=img[pos-pixelBytes+offset];
        }
        if (x<width-1) {
            neighbours[2]=img[pos-width*pixelBytes+pixelBytes+offset];
            neighbours[3]=img[pos+pixelBytes+offset];
        }
    }
    neighbours[8]=img[pos+offset];
    int min = neighbours[0];
    for (int i=1;i<=8;i++){
        if (neighbours[i]>min){
            min = neighbours[i];
        }
    }
    return min;
}



/***************************************************************
bool SaveBMP ( BYTE* Buffer, int width, int height, 
		long paddedsize, LPCTSTR bmpfile )

Function takes a buffer of size <paddedsize> 
and saves it as a <width> * <height> sized bitmap 
under the supplied filename.
On error the return value is false.

***************************************************************/

bool SaveBMP ( BYTE* Buffer, int width, int height, long paddedsize, LPCTSTR bmpfile )
{
    if (bw) {
        SaveBW(Buffer,width,height,paddedsize,bmpfile);
        return true;
    }
	// declare bmp structures 
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER info;
	
	// andinitialize them to zero
	memset ( &bmfh, 0, sizeof (BITMAPFILEHEADER ) );
	memset ( &info, 0, sizeof (BITMAPINFOHEADER ) );
	
	// fill the fileheader with data
	bmfh.bfType = 0x4d42;       // 0x4d42 = 'BM'
	bmfh.bfReserved1 = 0;
	bmfh.bfReserved2 = 0;
	bmfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + paddedsize;
	bmfh.bfOffBits = 0x36;		// number of bytes to start of bitmap bits
	
	// fill the infoheader

	info.biSize = sizeof(BITMAPINFOHEADER);
	info.biWidth = width;
	info.biHeight = height;
	info.biPlanes = 1;			// we only have one bitplane
	info.biBitCount = 24;		// RGB mode is 24 bits
	info.biCompression = BI_RGB;	
	info.biSizeImage = 0;		// can be 0 for 24 bit images
	info.biXPelsPerMeter = 0x0ec4;     // paint and PSP use this values
	info.biYPelsPerMeter = 0x0ec4;     
	info.biClrUsed = 0;			// we are in RGB mode and have no palette
	info.biClrImportant = 0;    // all colors are important

	// now we open the file to write to
	HANDLE file = CreateFile ( bmpfile , GENERIC_WRITE, FILE_SHARE_READ,
		 NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
	if ( file == NULL )
	{
		CloseHandle ( file );
		return false;
	}
	
	// write file header
	unsigned long bwritten;
	if ( WriteFile ( file, &bmfh, sizeof ( BITMAPFILEHEADER ), &bwritten, NULL ) == false )
	{	
		CloseHandle ( file );
		return false;
	}
	// write infoheader
	if ( WriteFile ( file, &info, sizeof ( BITMAPINFOHEADER ), &bwritten, NULL ) == false )
	{	
		CloseHandle ( file );
		return false;
	}
	// write image data
	if ( WriteFile ( file, Buffer, paddedsize, &bwritten, NULL ) == false )
	{	
		CloseHandle ( file );
		return false;
	}
	
	// and close file
	CloseHandle ( file );

	return true;
}

/*******************************************************************
BYTE* LoadBMP ( int* width, int* height, long* size 
		LPCTSTR bmpfile )

The function loads a 24 bit bitmap from bmpfile, 
stores it's width and height in the supplied variables
and the whole size of the data (padded) in <size>
and returns a buffer of the image data 

On error the return value is NULL. 

  NOTE: make sure you [] delete the returned array at end of 
		program!!!
*******************************************************************/

BYTE* LoadBMP ( int* width, int* height, long* size, LPCTSTR bmpfile )
{
	// declare bitmap structures
	BITMAPFILEHEADER bmpheader;
	BITMAPINFOHEADER bmpinfo;
	// value to be used in ReadFile funcs
	DWORD bytesread;
	// open file to read from
	HANDLE file = CreateFile ( bmpfile , GENERIC_READ, FILE_SHARE_READ,
		 NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL );
	if ( NULL == file )
		return NULL; // coudn't open file
	

	// read file header
	if ( ReadFile ( file, &bmpheader, sizeof ( BITMAPFILEHEADER ), &bytesread, NULL ) == false )
	{
        CloseHandle ( file );
		return NULL;
	}

	//read bitmap info

	if ( ReadFile ( file, &bmpinfo, sizeof ( BITMAPINFOHEADER ), &bytesread, NULL ) == false )
	{
		CloseHandle ( file );
		return NULL;
	}
	
	// check if file is actually a bmp
	if ( bmpheader.bfType != 'MB' )
	{
		CloseHandle ( file );
		fprintf(stderr, "File is not BMP image!\n");
		return NULL;
	}

	// get image measurements
	*width   = bmpinfo.biWidth;
	*height  = abs ( bmpinfo.biHeight );

	// check if bmp is uncompressed
	if ( bmpinfo.biCompression != BI_RGB )
	{
		fprintf(stderr, "BMP is compressed!\n");
		CloseHandle ( file );
		return NULL;
	}

	// check if we have 24 bit bmp
	if ( bmpinfo.biBitCount != 24 )
    {
        char mbsname[255];
        wcstombs(mbsname,bmpfile,254);
        CloseHandle ( file );
        *size=bmpinfo.biWidth*bmpinfo.biHeight;
        return LoadBW(mbsname,*size);

    } else {
        bw = false;
    }
	// create buffer to hold the data
	*size = bmpheader.bfSize - bmpheader.bfOffBits;
	BYTE* Buffer = new BYTE[ *size ];
	// move file pointer to start of bitmap data
	SetFilePointer ( file, bmpheader.bfOffBits, NULL, FILE_BEGIN );
	// read bmp data
	if ( ReadFile ( file, Buffer, *size, &bytesread, NULL ) == false )
	{
		delete [] Buffer;
		CloseHandle ( file );
		return NULL;
	}

	// everything successful here: close file and return buffer
	
	CloseHandle ( file );

	return Buffer;
}










void SaveBW(BYTE * Buffer, int width, int height, long paddedsize, LPCTSTR bmpfile)
{

    FILE * dst = fopen("output.bmp","wb");

    fwrite(bwHeader, 0x436, 1, dst);
    fwrite(Buffer, height*width, 1, dst);

    fclose(dst);
}

BYTE * LoadBW(char * name,int size){
    FILE * src = fopen(name, "rb");
    fread(bwHeader, 0x436, 1, src);
    BYTE * buffer = (BYTE*)malloc(size);
    fread(buffer,size,1,src);
    fclose(src);
    bw = true;
    return buffer;
}

bool isBW(){
    return bw;
}
