#include "warp.h"

warp::warp(int inputs)
{
    //hidden = neuronLayer(36,36);
    //exit = neuronLayer(36,5);
    //hidden.init();
    exit.init();
}

const double *warp::process(const double *input)
{
    memcpy(hidden.inputSignals,input,sizeof(double)*hidden.inputs);
    hidden.passHidden();
    memcpy(exit.inputSignals,hidden.output,sizeof(double)*5);
    exit.pass();
    return exit.output;
}

void warp::addLearnImage(double * input, double *output)
{
    in.push_back(input);
    out.push_back(output);
}

void warp::learn()
{
    hidden.learnHidden(in);
    for(int i=0;i<10;i++){
        for (int j=0;j<in.size();j++){

            memcpy(hidden.inputSignals,in[j],sizeof(double)*hidden.inputs);


            hidden.passHidden();
            memcpy(exit.inputSignals,hidden.output,sizeof(double)*5);
            exit.pass();
//            for(int i=0;i<6;i++){
//                for(int j=0;j<6;j++){
//                    cout<<exit.output[i*4+j]<<"  ";
//                }
//                cout<<endl;
//            }
            //memcpy(coefTmp,exit.coef,5*36*sizeof(double));

            exit.learn(exit.inputSignals,out[j]);

        }
    }
}
