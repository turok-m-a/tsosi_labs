#include "neuronlayer.h"

neuronLayer::neuronLayer()
{

}

neuronLayer::neuronLayer(int a,int b)
{
    //gen = new mt19937(rd());
    //dis = new uniform_real_distribution<>(-1, 1);
    NEUR_NUM = b;
    INPUTS = a;
    inputSignals = (double *)malloc(INPUTS*sizeof(double));
    inputs = INPUTS;
    output = (double *)malloc(NEUR_NUM*sizeof(double));
//    coef = (double **)malloc(NEUR_NUM*sizeof(double));
//    for (int i=0;i<NEUR_NUM;i++){
//        coef[i] = (double *)calloc(INPUTS,sizeof(double));
//    }
    step = (double *)malloc(NEUR_NUM*sizeof(double));
    deltaForPrevLayer = (double *)malloc(NEUR_NUM*sizeof(double));
}

void neuronLayer::init()
{
    for(int i=0;i<NEUR_NUM;i++){
        for(int j=0;j<INPUTS;j++){
        //coef[i][j]=(*dis)(*gen);
            coef[i][j]=((double)(rand()%2000)/1000)-1;
        }

        step[i]=((double)(rand()%2000)/1000)-1;
        //cout<<"step"<<step[i]<<endl;
        //step[i]=(*dis)(*gen);
    }
}

double *neuronLayer::pass()
{
    for(int i=0;i<NEUR_NUM;i++){
        double sum=0;
        for(int j=0;j<INPUTS;j++){
            sum+=inputSignals[j]*coef[i][j];
        }
        //output[i]=sum;
        //sum+=step[i];//упрощенная формула для РБФ
        output[i]=sum;
    }
    return output;
}

double *neuronLayer::passHidden()
{
    for(int i=0;i<NEUR_NUM;i++){
        double sum=0;
        for(int j=0;j<INPUTS;j++){
            //sum+=inputSignals[j]*coef[i][j];
        }
        //output[i]=sum;
        //sum+=step[i];
        output[i]=exp(-1*pow(vectorDistance(inputSignals,clusterCenter[i],INPUTS),2)/skoSqr[i]);
    }
    return output;
}
double neuronLayer::vectorDistance(double*a,double*b,int n)
{
    double sum=0;
    for(int i=0;i<n;i++){
        sum+=pow((a[i]-b[i]),2);
    }
    return sqrt(sum);
}

void neuronLayer::learn(double * input,double * input2)//input2 - требуемый выход
{
    for(int i=0;i<NEUR_NUM;i++){
        //deltaForPrevLayer[i]=0;
        for(int j=0;j<INPUTS;j++){
        //coef[i][j]=coef[i][j]+exitLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i])*input[j];
        coef[i][j]=coef[i][j]+exitLearnSpeed*(input2[i]-output[i])*input[j];//упрощенная формула для РБФ
        }
        step[i]=step[i]+exitLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i]);
       deltaForPrevLayer[i]=exitLearnSpeed*output[i]*(1.0-output[i])*(input2[i]-output[i]);
    }
   // cout<<"exit error "<<input2[0]-output[0]<<endl;
}
void neuronLayer::learnHidden(vector<double*> clusterCenter_)
{
    for(int i=0;i<5;i++){
   for(int j=0;j<INPUTS;j++){
        clusterCenter[i][j]=clusterCenter_[i][j];
   }
   if (i>0) cout <<vectorDistance(clusterCenter[i],clusterCenter[i-1],36)<<endl;
   }
   for(int i=0;i<5;i++){
        double min=10000.0;
        for(int j=0;j<5;j++){
            double vd=vectorDistance(clusterCenter[i],clusterCenter[j],36);
            if (vd<min && i!=j){
                min=vd;
            }
        }
        skoSqr[i]=pow(min,2);
   }
   // cout<<"hidden error "<<input2[0]-output[0]<<endl;
}
