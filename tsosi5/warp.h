#ifndef WARP_H
#define WARP_H
#include <neuronlayer.h>
#include <vector>
using namespace std;
class warp
{
private:
    neuronLayer hidden = neuronLayer(36,5);
            neuronLayer exit = neuronLayer(5,5);
public:
    warp(int inputs);
    const double *process(const double *);
    void addLearnImage(double*,double*output);
    void learn();
    vector<double*> in;
    vector<double*> out;
};

#endif // WARP_H
